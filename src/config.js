const config = {
  client: {
    minVersion: 0.01,
    currentVersion: 0.02
  },
  redis: {
    port: process.env['REDIS_PORT'],
    host: process.env['REDIS_HOST'],
    db: '0'
  }
};
export default config;
