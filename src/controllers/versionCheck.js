const versionCheck = (req, res) => {
  res.writeHead(200, { 'Content-Type' : 'text/html' });
  res.end('<h1>Version Check</h1>');
}

export default versionCheck;
