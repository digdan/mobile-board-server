import axios from 'axios';

const geoip = ip => {
  const url = `http://ipinfo.io/${ip}`;
  axios.get(url).then( response => {
    console.log('geoip', response);
  });
}

export {
  geoip as default
}
