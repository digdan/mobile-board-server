import _ from 'lodash';
import clientHandler from '../clientHandler/clientHandler';

class Session {
  constructor(props) {
    this.ws = props.ws;
    this.req = props.req;
    this.redis = props.redis;
    this.client = clientHandler(props);
		this.client.on('message', message => {
			this.digestMessage(message);
		});
  }

	digestMessage(message) {
    console.log('session handler message', message);
		this.redis.zadd('sessions', [Date.now(), this.address], (error, result) => {
      if (error) {
        console.log('Session update error', error);
      }
    }); //Update TTL from score
	}

}

const sessionHandler = props => {
	return new Session(props);
}

export {
  sessionHandler as default
};
