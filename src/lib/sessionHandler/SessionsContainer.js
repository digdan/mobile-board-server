import _ from 'lodash';
import clientHandler from '../clientHandler/clientHandler';
import sessionHandler from './sessionHandler';
import EventEmitter from 'events';

const SESSION_CLEAN_INTERVAL = 5000; // 5 seconds
const SESSION_CLEAN_THRESHOLD = 10000; // Inactivity until session is terminated

class SessionsContainer extends EventEmitter {
  constructor(props) {
    super();
    this.sessions = [];
		this.redis = props.redis;
    this.sessionCleaner();
  }

  newSession(props) {
    this.sessions.push(sessionHandler(props));
  }

  sessionCleaner() {
  	setInterval( () => {
      const threshold = Date.now() - SESSION_CLEAN_THRESHOLD;
  		this.redis.zrange('sessions', 0, threshold, (error, result) => {
        // TODO - send logoff event for expired sessions
  			this.sessions = [];
  		});
    }, SESSION_CLEAN_INTERVAL);
  }

}

export {
  SessionsContainer as default
};
