import winston from 'winston';

const alignedWithColorsAndTime = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);

const logger = winston.createLogger({
  level: 'info',
  format: alignedWithColorsAndTime,
  transports: [
    new winston.transports.Console()
  ]
});

export {
  logger as default
}
