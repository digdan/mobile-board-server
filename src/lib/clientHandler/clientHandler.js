import _ from 'lodash';
import msgpack from 'msgpack-lite';
import cypht from 'cyphtjs';
import config from '../../config';
import basex from 'base-x';
import EventEmitter from 'events';
import logger from '../logger';

//JSON friendly base encoding
const BASE91 = basex('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~"');
const BASE61 = basex('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');

const readyStates = {
	connecting: 0,
	open: 1,
	closing: 2,
	closed: 3
}

const actions = {
	serverKey: serverKey => {
		return {
			type: 'SERVER_KEY',
			data: serverKey
		}
	},
	bootClient: reason => {
		return {
			type:'SERVER_BOOT',
			data: reason
		}
	}
};

class Client extends EventEmitter {
  constructor(props) {
		super();
    let clientKey = new cypht.CyphtPublicKey();
    clientKey.importRaw(BASE61.decode((props.req.url.substring(1))));
		this.props = {
			...props,
			serverKey: [null, null],
			id: this.generateId(),
			clientKey
		};

    this.props.ws.on('message', message => {
      this.onMessage(message);
    });

		logger.info(`Generating client keys ${this.props.id}`);
    cypht.generateKeys().then( keys => {
			logger.info(`Client keys generated ${this.props.id}`);
      this.props.serverKeys = keys;
      this.send(
				actions.serverKey(this.props.serverKeys.publicKey.exportRaw())
      );
    });
  }

  onMessage = encodedData => {
    this.cypht.decode(BASE91.decode(encodedData)).then( packedMessage => {
			this.emit('message', msgpack.decode(packedMessage));
    })
  }

  onDisconnect = () => {
		this.emit('disconnect', this.props.id);
  }

  send = message => {
		if (this.props.ws.readyState !== readyStates.open) {
				return false;
		}
    cypht.encypht(msgpack.encode(message), this.props.clientKey).then( emsg => {
      this.props.ws.send(BASE91.encode(emsg));
      this.emit('sent', emsg);
    })
  }

  generateId = () => {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4();
  };

	boot = reason => {
			this.send(actions.bootClient(reason));
	}

}

//Factory
const clientHandler = props => {
  return new Client(props);
}

export {
  clientHandler as default
}
