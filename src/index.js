import _ from 'lodash';
import dotenv from 'dotenv';
import http from 'http';
import WebSocket from 'ws';
import redisio from 'ioredis';
import config from './config';
import routes from './routes';
import logger from './lib/logger';
import SessionsContainer from './lib/sessionHandler/SessionsContainer';

dotenv.config();

const redis = new redisio(config.redis);
const server = http.createServer( routes );
const wss = new WebSocket.Server({ server });
let container = new SessionsContainer({redis});

wss.on('connection', (ws, req) => {
	logger.info('New connection');
	container.newSession({ ws, req, redis });
});

let listenPort = process.env['PORT'] || 8880;

logger.info(`Socket server listening on port ${listenPort}`);

server.listen(listenPort);
