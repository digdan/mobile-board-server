import _ from 'lodash';
import versionCheck from './controllers/versionCheck';
let routesTable = [];

routesTable.push({
  url: '/versionCheck',
  controller: versionCheck
})

const routes = (req, res) => {
  const foundRoute = _.find(routesTable, { url : req.url })
  if (typeof foundRoute !== 'undefined') {
    foundRoute.controller(req, res);
  } else {
    res.writeHead(404, { 'Content-type' : 'text/html'})
    res.end('<h1>404 Not Found</h1>'+req.url)
  }

}
export default routes;
